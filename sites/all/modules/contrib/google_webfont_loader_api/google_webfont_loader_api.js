/**
 * @file google_webfont_loader_api.js
 * This file retrieves the webfont js file from google if necessary and loads
 * the webfont loader with the appropriate file callbacks.
 */
(function ($) {
  Drupal.behaviors.google_webfont_loader = {
    attach: function() {
      $('body').once('google-webfont-loader', function() {
        WebFont.load(Drupal.settings.google_webfont_loader_api_setting);
      });
    }
  }
})(jQuery);
