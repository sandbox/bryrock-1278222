<?php

class context_daylight extends context_condition {

  function condition_form($context) {
    $defaults = $this->fetch_from_context($context);

    if (isset($defaults['values']))
      $defaults = $defaults['values'];

    return array(
      'daytime' => array(
        '#title' => t('Day Time?'),
        '#type' => 'checkbox',
        '#default_value' => isset($defaults['daytime']) ? $defaults['daytime'] : 0,
      ),
      'latitude' => array(
        '#title' => t('Latitude'),
        '#type' => 'textfield',
        '#default_value' => isset($defaults['latitude']) ? $defaults['latitude'] : 0,
      ),
      'longitude' => array(
        '#title' => t('Longitude'),
        '#type' => 'textfield',
        '#default_value' => isset($defaults['longitude']) ? $defaults['longitude'] : 0,
      ),
      'timezone' => array(
        '#title' => t('Time Zone'),
        '#type' => 'textfield',
        '#default_value' => isset($defaults['timezone']) ? $defaults['timezone'] : 0,
      ),
    );
  }

  // this is stupid, why is the default implementation not doing this?
  function condition_form_submit($values) {
    return $values;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
      $values = $this->fetch_from_context($context, 'values');

      $sunrise = date_sunrise(time(), SUNFUNCS_RET_TIMESTAMP, $values['latitude'], $values['longitude'], 90, $values['timezone']);
      $sunset = date_sunset(time(), SUNFUNCS_RET_TIMESTAMP, $values['latitude'], $values['longitude'], 90, $values['timezone']);

      $current = time();

      if ($values['daytime']) {
        if ($current < $sunset && $current > $sunrise) {
          $this->condition_met($context);
        }
      } else {
        if ($current > $sunset && $current < ($sunrise + 86400)) {
          $this->condition_met($context);
        }
      }
    }
  }
}