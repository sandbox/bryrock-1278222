<?php

/**
 * @file
 * Administration page callbacks for the nashdrupalers module.
 * @author Bryan Lewellen <bryrock@raindearmedia.com>
 * @since 09/26/2011
 * @version 0.1
 *
*/

/**
* Form builder. Configure annotations.
*
* @ingroup forms
* @see system_settings_form().
*/

function nashdrupalers_admin_settings($form, &$form_state) {
	// Misc. Stuff


  $form['core'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nashville Drupalers'),
    '#description' => t('Miscellaneous settings for Nashville Drupalers'),
    '#collapsible' => FALSE,
  );

  $form['core']['nashdrupalers_day_status'] = array(
    '#type' => 'radios',
    '#title' => t('Do you want to have a good day?'),
    '#description' => t('Determines whether or not you are likely to have a good day.'),
    '#default_value' => variable_get('nashdrupalers_day_status', 0),
    '#options' => array(
      t('Disabled (never)'),
      t('Most of the time'),
      t('Always'),
    ),
   );

	$form[ 'submit' ] = array(
    '#type' => 'submit',
    '#value' => t( 'Submit' )
  );

  return ($form);
}

/**
 * _submission function for nashdrupalers_admin_settings
 * @param	array $form
 * @param	array $form_state
 */
function nashdrupalers_admin_settings_submit( $form, &$form_state ) {

	if ($form_state[ 'values' ][ 'nashdrupalers_day_status' ]) {  
 		drupal_set_message('Great!  We\'ll see what we can do.');
  }
  else {
    drupal_set_message('You sure?  Think it over and try again.', 'warning');
  }
} 

