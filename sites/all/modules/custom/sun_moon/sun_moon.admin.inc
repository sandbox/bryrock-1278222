<?php

/**
 * @file
 * Administration page callbacks for the sun_moon module.
 * @author Bryan Lewellen <bryrock@raindearmedia.com>
 * @since 09/20/2011
 * @version 0.1
 *
 * Thanks to Tom Camp for the nifty lat/long lookup by zipcode code.
 */

/**
 * Form builder. Configure annotations.
 *
 * @ingroup forms
 * @see system_settings_form()
 */


function sun_moon_admin_settings($form, &$form_state) {
  // Activate daylight checking and determine latitude and longitude,
  // either by US zip code or manual entry.

  $lat = variable_get( 'sun_moon_latitude', SUNMOON_SITE_LATITUDE );
  $lon = variable_get( 'sun_moon_longitude', SUNMOON_SITE_LONGITUDE );
  $tmz = variable_get( 'sun_moon_timezone', SUNMOON_SITE_LONGITUDE );
  $how = variable_get( 'sun_moon_method', SUNMOON_SITE_METHOD_ZIP );


  if ( $lat && $lon && $tmz ) {
    $form[ 'latlontmz' ] = array(
      '#value' => t( 'Latitude %lat, Longitude %lon, Timezone %tmz', array( '%lat' => $lat, '%lon' => $lon, '%tmz' => $tmz ) )
    );
  }


  if (!$_POST && (!variable_get('sun_moon_check', SUNMOON_CHECK_NONE))) {
    drupal_set_message(t('Site daylight checking is currently disabled; enter your location information below and enable Sun & Moon Checking.'), 'error');
  }

  $form['core'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sun & Moon'),
    '#description' => t('Sun & Moon stores system-wide accessible information on the current status of the sun (risen or set) and moon to enable triggers and actions.  For instance, you could change a theme based on whether the sun was up or down or on the current phase of the moon.'),
    '#collapsible' => FALSE,
  );

  $form['core']['sun_moon_check'] = array(
    '#type' => 'radios',
    '#title' => t('Sun & Moon Checking'),
    '#description' => t('Whether or not site Sun & Moon checking is active.'),
    '#default_value' => variable_get('sun_moon_check', SUNMOON_CHECK_NONE),
    '#options' => array(
      t('Disabled (never)'),
      t('Check on Cron only'),
      t('Check on any user login'),
      t('Check Both (on cron & any user login)'),
    ),
    '#description' => t('Determines whether or not Sun & Moon checking is active.'),
  );

  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => t('Location Settings'),
    '#description' => t('Location information required to localize sun information to your site (defaults are for Murfreesboro, TN).'),
    '#collapsible' => TRUE,
  );

  $form['location']['sun_moon_method'] = array(
    '#type' => 'radios',
    '#title' => t('Calculate latitude/longitude from Zipcode (US only)'),
    '#description' => t('Calculate latitude/longitude from Zipcode (US only or enter values'),
    '#default_value' => variable_get('sun_moon_method', SUNMOON_SITE_METHOD_ZIP),
    '#options' => array(
      t('Enter lat/long values'),
      t('Lookup by Zipcode'),
    ),
  );


  $form['location']['sun_moon_zip'] = array(
    '#type' => 'textfield',
    '#title' => t( 'Enter Zip Code' ),
    '#default_value' => variable_get( 'sun_moon_zip', SUNMOON_SITE_ZIPCODE ),
    '#size' => 6,
    '#maxlength' => 5,
    '#description' => t( '5 digit Zip code.' ),
  );

  $form['location']['sun_moon_latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#description' => t('Enter your local latitude, exact to at least two points (default example: 35.85).'),
    '#default_value' => variable_get('sun_moon_latitude', SUNMOON_SITE_LATITUDE),
  );

  $form['location']['sun_moon_longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#description' => t('Enter your local longitude, exact to at least two points (default example: -86.39).'),
    '#default_value' => variable_get('sun_moon_longitude', SUNMOON_SITE_LONGITUDE),
  );

  $form['location'] ['sun_moon_timezone'] = array(
    '#type' => 'textfield',
    '#title' => t('Time Zone Offset'),
    '#description' => t('Enter your local timezone in offset to GMT (example: -6).'),
    '#default_value' => variable_get('sun_moon_timezone', SUNMOON_SITE_TIMEZONE),
  );

    $form[ 'submit' ] = array(
    '#type' => 'submit',
    '#value' => t( 'Submit Settings' )
  );

  return ($form);
}

/**
 * Form API callback to validate the sun_moon settings form.
 */
function sun_moon_admin_settings_validate($form, &$form_state) {
  $activate  = $form_state['values']['sun_moon_check'];
  $zipmethod = $form_state['values']['sun_moon_method'];
  $tzoffset = (intval($form_state['values']['sun_moon_timezone']));
  $tzoffset = (int)$tzoffset;

  if ($zipmethod) {

    if ( drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) < 5 || drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) > 10 || ( drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) < 10 && drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) > 5 )  ) {
    form_set_error( 'sun_moon_zip', t( 'The zip code you entered is not valid.' ) );
    $valid = FALSE;
    }
    elseif ( is_numeric( $form_state[ 'values' ][ 'sun_moon_zip' ] ) && drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) == 5 ) {
    $valid = TRUE;
    }
    elseif ( drupal_strlen( $form_state[ 'values' ][ 'sun_moon_zip' ] ) == 10 ) {
      if ( preg_match( '/^[0-9]{5}([- ]?[0-9]{4})?$/', $form_state[ 'values' ][ 'sun_moon_zip' ] ) ) {
      $valid = TRUE;
      }
      else {
      form_set_error( 'sun_moon_zip', t( 'The zip code you entered is not valid.' ) );
      form_set_error('sun_moon_check', t('You have not activated Sun & Moon.'));
      $valid = FALSE;
      }
    }

    if ( $valid ) {
    drupal_set_message( t( 'Zip Code set to %z.', array( '%z' => $form_state[ 'values' ][ 'sun_moon_zip' ] ) ), 'status', FALSE );
    variable_set( 'sun_moon_zip', str_ireplace( ' ', '-', $form_state[ 'values' ][ 'sun_moon_zip' ] ) );
    }
  }
  else {

    if (!$form_state['values']['sun_moon_latitude']) {
    form_set_error('sun_moon_check', t('You must enter a latitude to activate Sun & Moon.'));
    }

    if (!$form_state['values']['sun_moon_longitude']) {
    form_set_error('sun_moon_check', t('You must enter a longitude to activate Sun & Moon.'));
    }

  }

  if ( !(($tzoffset >= -12) && ($tzoffset <= 14)) && $activate )  {
    if ( ! (($tzoffset >= -12) && ($tzoffset <= 14)) ) {
      form_set_error('sun_moon_check', t('You must enter a time zone offset between -12 and 14 to activate Sun & Moon.'));
    }
    else {
      if (!$activate) {
        form_set_error('sun_moon_check', t('You have not activated Sun & Moon.'));
        variable_set( 'sun_moon_check', $form_state[ 'values' ][ 'sun_moon_check' ]  );
      }
    }
  }

  drupal_set_message(t('Sun & Moon is now activated.'));
  variable_set( 'sun_moon_check', $form_state[ 'values' ][ 'sun_moon_check' ]  );
}

/**
 * _submition function for moon_phases_admin_form
 * @param array $form
 * @param array $form_state
 */
function sun_moon_admin_settings_submit( $form, &$form_state ) {
/*  ?><div><?php krumo ($form_state[ 'values' ][ 'sun_moon_check' ]) ; ?></div><?php */

  if ($form_state[ 'values' ][ 'sun_moon_method' ]) {
    sun_moon_admin_get_latlon( $form_state[ 'values' ][ 'sun_moon_zip' ] );
  }
  else {
    variable_set( 'sun_moon_latitude', $form_state[ 'values' ][ 'sun_moon_latitude' ]  );
    variable_set( 'sun_moon_longitude', $form_state[ 'values' ][ 'sun_moon_longitude' ]  );
  }
//  variable_set( 'sun_moon_latitude', $form_state[ 'values' ][ 'sun_moon_latitude' ]  );
//  variable_set( 'sun_moon_longitude', $form_state[ 'values' ][ 'sun_moon_longitude' ]  );
  variable_set( 'sun_moon_timezone', $form_state[ 'values' ][ 'sun_moon_timezone' ]  );
  variable_set( 'sun_moon_check', $form_state[ 'values' ][ 'sun_moon_check' ]  );
  variable_set( 'sun_moon_method', $form_state[ 'values' ][ 'sun_moon_method' ]  );

  sun_moon_sun_lookup(); // retrieve sunrise and sunset times, then report back via messaging.

  if (variable_get('sun_moon_sun_up', NULL)) {
    drupal_set_message(t('Today\'s Sun for ' . variable_get('sun_moon_zip', SUNMOON_SITE_ZIPCODE ) . ' has risen!'));
  }
  else {
    if (time() < (variable_get('sun_moon_sun_rise_today', NULL))) {
      drupal_set_message(t('Today\'s Sun for ' . variable_get('sun_moon_zip', SUNMOON_SITE_ZIPCODE ) . ' has not yet risen.'));
    }
    else {
      drupal_set_message(t('Today\'s Sun for ' . variable_get('sun_moon_zip', SUNMOON_SITE_ZIPCODE ) . ' has set.'));
    }
  }
  drupal_set_message(t('True Sunrise for ' . date('l M j, Y') . ' is ' . date('h:i A', (variable_get('sun_moon_sun_rise_today', NULL))) . '.'));
  drupal_set_message(t('True Sunset for ' . date('l M j, Y') . ' is ' . date('h:i A', (variable_get('sun_moon_sun_set_today', NULL))) . '.'));

  drupal_set_message(t('Civilian Twilight for ' . date('l M j, Y') . ' begins at ' . date('h:i A', (variable_get('sun_moon_civilian_twilight_start', NULL))) . '.'));

  drupal_set_message(t('Civilian Twilight for ' . date('l M j, Y') . ' ends at ' . date('h:i A', (variable_get('sun_moon_civilian_twilight_end', NULL))) . '.'));

    drupal_set_message(t('Nautical Twilight for ' . date('l M j, Y') . ' begins at ' . date('h:i A', (variable_get('sun_moon_nautical_twilight_start', NULL))) . '.'));

  drupal_set_message(t('Nautical Twilight for ' . date('l M j, Y') . ' ends at ' . date('h:i A', (variable_get('sun_moon_nautical_twilight_end', NULL))) . '.'));

    drupal_set_message(t('Astronomical Twilight for ' . date('l M j, Y') . ' begins at ' . date('h:i A', (variable_get('sun_moon_astro_twilight_start', NULL))) . '.'));

  drupal_set_message(t('Astronomical Twilight for ' . date('l M j, Y') . ' ends at ' . date('h:i A', (variable_get('sun_moon_astro_twilight_end', NULL))) . '.'));


  drupal_set_message(t('Current time is ' . date('l M j, Y h:i A') . '.'));
}

/**
 * Get the latitude and longitude for the zip code entered from NOAA
 * @param  string   $zip - Zip Code
 * @return boolean  TRUE
 *
 * For more information on how to do this, see http://graphical.weather.gov/xml/
 */
function sun_moon_admin_get_latlon( $zip ) {

  $base_url = 'http://www.weather.gov/forecasts/xml/DWMLgen/wsdl/ndfdXML.wsdl';
  $parameters = array( 'zipCodeList' => str_ireplace( ' ', '-', $zip ) );

  $client = new SoapClient( $base_url );

  try {
    $results = $client->__soapCall( 'LatLonListZipCode', $parameters );
    $data = get_object_vars( simplexml_load_string( $results ) );
    $lat_long = explode( ',', $data[ 'lat_longList' ] );

    variable_set( 'sun_moon_latitude', $lat_long[ 0 ] );
    variable_set( 'sun_moon_longitude', $lat_long[ 1 ] );
    drupal_set_message( t( 'Latitude set to %lat and longitude set to %lon.', array( '%lat' => $lat_long[ 0 ], '%lon' => $lat_long[ 1 ] ) ), 'status', FALSE );
  }
  catch ( Exception $e ) {
    drupal_set_message( t( 'Error retrieving data. Exception caught: %e', array( '%e' => $e->getMessage() ) ) );
  }

  return TRUE;

}
