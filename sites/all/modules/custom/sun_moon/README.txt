Some Testing Notes:

Latitude, Longitude & timezone for Murfreesboro, TN



Lat = 35.89

Long = -86.45

TZ = -6

Project Sun & Moon Goals
---------------------

What this will do.

#1. It will determine the daily sunrise and sunset times for the site. (done)

#2. It will determine the daily sunrise and sunset times for a user. (to-do)

#3.  It will find the lat and long for the site/user by zipcode (US only).  Others can enter their own lat long. (done)

#4.  It will work with a context plug-in that will switch themes based on site or user day cycles. (done - site) (user - todo)

#5.  It will eventually also report moon phases. (done)

#6.  I may also eventually report moon names and blue moons.(to do)

#7.  It may also provide theme changing based on moon phases. (maybe, maybe, maybe -- 29 moons was hard enough)


-------

How this will happen:


Sun Cycles (site): Site admin selects site zip code.
          					Sun cycles determined at cron.  A boolean is set for sun up.
          					
            (user):  Site admin selects whether this option is available to users.
                     Users must enter zip code.
                     
                     If option made available, user sun cycles determined at cron and store in db.
                     A boolean (for each user) is set for sun up.
                     A boolean (for each user) is set to indicate whether or not they are using this feature.
                     
                     
                     
