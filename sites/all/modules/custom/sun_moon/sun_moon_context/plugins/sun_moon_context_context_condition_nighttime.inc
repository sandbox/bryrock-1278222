<?php

/**
 * @file
 * Code for "Nighttime" Context Condition
 *
 * Tests for any time between sunset and sunrise.
 */


/**
 * Trigger in-active (but remains as an option).
 */
define('NIGHTTIME_INACTIVE', 1);

/**
 * Trigger is active.
 */
define('NIGHTTIME_ACTIVE', 2);

class sun_moon_context_context_condition_nighttime extends context_condition {

  function condition_form($context) {
    $defaults = $this->fetch_from_context($context);

    if (isset($defaults['values']))
      $defaults = $defaults['values'];

    return array(
      'sun_moon_nighttime' => array(
        '#title' => t('Site Nighttime'),
        '#description' => t('Only TRUE between Sunset and Sunrise (based on the site\'s location).'),
        '#type' => 'radios',
        '#options' => array(
          NIGHTTIME_ACTIVE => t('active'),
          NIGHTTIME_INACTIVE => t('inactive'),
        ),
        '#default_value' => isset($defaults['sun_moon_nighttime']) ? $defaults['sun_moon_nighttime'] : 0,
      ),
    );
  }

  function condition_form_submit($values) {
    return $values;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
/*      ?><div><?php krumo($context);?></div><?php */

      $values = $this->fetch_from_context($context, 'values');

/*       ?><div><?php krumo($values);?></div><?php */

      if ($values['sun_moon_nighttime'] == NIGHTTIME_ACTIVE) {
        if (!variable_get('sun_moon_sun_up', FALSE)) {
          $this->condition_met($context);
        }
      }
    }
  }
}
