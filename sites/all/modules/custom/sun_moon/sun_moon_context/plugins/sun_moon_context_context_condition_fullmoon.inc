<?php

/**
 * @file
 * Code for "Fullmoon" Context Condition
 *
 * Tests for a full moon.
 */


/**
 * Trigger in-active (but remains as an option).
 */
define('FULLMOON_INACTIVE', 1);

/**
 * Trigger is active.
 */
define('FULLMOON_ACTIVE', 2);

class sun_moon_context_context_condition_fullmoon extends context_condition {

  function condition_form($context) {
    $defaults = $this->fetch_from_context($context);

    if (isset($defaults['values']))
      $defaults = $defaults['values'];

    return array(
      'sun_moon_fullmoon' => array(
        '#title' => t('Full Moon'),
        '#description' => t('Only TRUE during a Full Moon.'),
        '#type' => 'radios',
        '#options' => array(
          FULLMOON_ACTIVE => t('active'),
          FULLMOON_INACTIVE => t('inactive'),
        ),
        '#default_value' => isset($defaults['sun_moon_fullmoon']) ? $defaults['sun_moon_fullmoon'] : 0,
      ),
    );
  }

  function condition_form_submit($values) {
    return $values;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
/*      ?><div><?php krumo($context);?></div><?php */

      $values = $this->fetch_from_context($context, 'values');

/*       ?><div><?php krumo($values);?></div><?php */

      if ($values['sun_moon_fullmoon'] == FULLMOON_ACTIVE) {
        if (variable_get('sun_moon_fullmoon', FALSE)) {
          $this->condition_met($context);
        }
      }
      }
  }
}
