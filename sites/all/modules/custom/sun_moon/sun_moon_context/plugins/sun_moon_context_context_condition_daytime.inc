<?php

/**
 * @file
 * Code for "Daytime" Context Condition
 *
 * Tests for any time between sunrise and sunset.
 */

/**
 * Trigger in-active (but remains as an option).
 */
define('DAYTIME_INACTIVE', 1);

/**
 * Trigger is active.
 */
define('DAYTIME_ACTIVE', 2);

class sun_moon_context_context_condition_daytime extends context_condition {

  function condition_form($context) {
    $defaults = $this->fetch_from_context($context);

    if (isset($defaults['values']))
      $defaults = $defaults['values'];

    return array(
      'sun_moon_daytime' => array(
        '#title' => t('Site Daytime'),
        '#description' => t('Only TRUE between Sunrise and Sunset (based on the site\'s location).'),
        '#type' => 'radios',
        '#options' => array(
          DAYTIME_ACTIVE =>   t('active'),
          DAYTIME_INACTIVE => t('inactive'),
        ),
        '#default_value' => isset($defaults['sun_moon_daytime']) ? $defaults['sun_moon_daytime'] : 0,
      ),
    );
  }

  function condition_form_submit($values) {
    return $values;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
/*      ?><div><?php krumo($context);?></div><?php */

      $values = $this->fetch_from_context($context, 'values');

/*       ?><div><?php krumo($values);?></div><?php */

      if ($values['sun_moon_daytime'] == DAYTIME_ACTIVE) {
        if (variable_get('sun_moon_sun_up', FALSE)) {
          $this->condition_met($context);
        }
      }
    }
  }
}
