<?php
/**
 * @file
 * nashdrupaler_user_profiles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function nashdrupaler_user_profiles_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_profile2_type().
 */
function nashdrupaler_user_profiles_default_profile2_type() {
  $items = array();
  $items['main'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "main",
    "label" : "Main profile",
    "weight" : "0",
    "data" : { "registration" : 1, "use_page" : 0 },
    "rdf_mapping" : [  ]
  }');
  return $items;
}
