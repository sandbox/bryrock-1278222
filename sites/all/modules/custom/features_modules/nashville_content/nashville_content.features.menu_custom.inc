<?php
/**
 * @file
 * nashville_content.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function nashville_content_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-member-content-menu.
  $menus['menu-member-content-menu'] = array(
    'menu_name' => 'menu-member-content-menu',
    'title' => 'Member Content Menu',
    'description' => 'Blogs and other member contributed content',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs and other member contributed content');
  t('Main menu');
  t('Member Content Menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');


  return $menus;
}
