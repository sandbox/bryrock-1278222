<?php
/**
 * @file
 * nashville_content.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function nashville_content_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_drupalers';
  $context->description = 'Shows Social Links in Sidebar';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*content/nashville-drupalers' => '*content/nashville-drupalers',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nashdrupalers-nashville_social' => array(
          'module' => 'nashdrupalers',
          'delta' => 'nashville_social',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Shows Social Links in Sidebar');
  t('theme');
  $export['about_drupalers'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_poll';
  $context->description = 'Getting Feedback';
  $context->tag = 'Blog Survey';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content/blog/undecided' => 'content/blog/undecided',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'poll-recent' => array(
          'module' => 'poll',
          'delta' => 'recent',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog Survey');
  t('Getting Feedback');
  $export['blog_poll'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = '';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
        '/user' => '/user',
        'login' => 'login',
        '*event*' => '*event*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-events-block' => array(
          'module' => 'views',
          'delta' => 'events-block',
          'region' => 'sidebar_first',
          'weight' => '-23',
        ),
        'views-tracker-block_1' => array(
          'module' => 'views',
          'delta' => 'tracker-block_1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'block-4' => array(
          'module' => 'block',
          'delta' => 4,
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => 5,
          'region' => 'preface_second',
          'weight' => '-10',
        ),
        'nashdrupalers-nashville_social' => array(
          'module' => 'nashdrupalers',
          'delta' => 'nashville_social',
          'region' => 'preface_third',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('theme');
  $export['front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'member_content';
  $context->description = 'Member Content & Blog blocks';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*blog*' => '*blog*',
        '*members*' => '*members*',
        '*FAQ' => '*FAQ',
        '*site-geist/meetings-past' => '*site-geist/meetings-past',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-member-content-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-member-content-menu',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'nashdrupalers-nashville_social' => array(
          'module' => 'nashdrupalers',
          'delta' => 'nashville_social',
          'region' => 'sidebar_first',
          'weight' => '5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Member Content & Blog blocks');
  t('theme');
  $export['member_content'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_default';
  $context->description = 'Default Block Sets';
  $context->tag = 'theme';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'content',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'user_search',
          'weight' => '-10',
        ),
        'nashdrupalers-nashville_simple_login' => array(
          'module' => 'nashdrupalers',
          'delta' => 'nashville_simple_login',
          'region' => 'user_second',
          'weight' => '-10',
        ),
        'views-events-block' => array(
          'module' => 'views',
          'delta' => 'events-block',
          'region' => 'sidebar_first',
          'weight' => '-24',
        ),
        'views-tracker-block_1' => array(
          'module' => 'views',
          'delta' => 'tracker-block_1',
          'region' => 'sidebar_first',
          'weight' => '-22',
        ),
        'user-new' => array(
          'module' => 'user',
          'delta' => 'new',
          'region' => 'postscript_third',
          'weight' => '-10',
        ),
        'user-online' => array(
          'module' => 'user',
          'delta' => 'online',
          'region' => 'postscript_fourth',
          'weight' => '-10',
        ),
        'system-powered-by' => array(
          'module' => 'system',
          'delta' => 'powered-by',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'nashdrupalers-theme_credit_nash_sky' => array(
          'module' => 'nashdrupalers',
          'delta' => 'theme_credit_nash_sky',
          'region' => 'footer_first',
          'weight' => '-9',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => 3,
          'region' => 'footer_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Default Block Sets');
  t('theme');
  $export['site_default'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_geist';
  $context->description = 'Blocks when viewing taxonomy pages';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*tags*' => '*tags*',
        '*site-geist*' => '*site-geist*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nashdrupalers-nashville_social' => array(
          'module' => 'nashdrupalers',
          'delta' => 'nashville_social',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks when viewing taxonomy pages');
  t('theme');
  $export['site_geist'] = $context;

  return $export;
}
