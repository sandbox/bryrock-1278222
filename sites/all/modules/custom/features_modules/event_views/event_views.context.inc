<?php
/**
 * @file
 * event_views.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function event_views_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content_events';
  $context->description = '';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*blog*' => '*blog*',
        '*content*' => '*content*',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'blog/member-content-index',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('theme');
  $export['content_events'] = $context;

  return $export;
}
