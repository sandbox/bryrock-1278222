<?php
/**
 * @file
 * daynnight_theme_changer.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function daynnight_theme_changer_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'daytime_theme';
  $context->description = 'Changes theme in accordance to daylight ';
  $context->tag = 'theme';
  $context->conditions = array(
    'sun_moon_daytime' => array(
      'values' => array(
        'sun_moon_daytime' => '2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sun_moon-sun_report' => array(
          'module' => 'sun_moon',
          'delta' => 'sun_report',
          'region' => 'postscript_first',
          'weight' => '-10',
        ),
        'sun_moon-moon_report' => array(
          'module' => 'sun_moon',
          'delta' => 'moon_report',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'nash-sky-day',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Changes theme in accordance to daylight ');
  t('theme');
  $export['daytime_theme'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'night_full_moon';
  $context->description = 'Adds HTML class (fullmoon) during Full Moon';
  $context->tag = 'theme';
  $context->conditions = array(
    'sun_moon_fullmoon' => array(
      'values' => array(
        'sun_moon_fullmoon' => '2',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'fullmoon',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds HTML class (fullmoon) during Full Moon');
  t('theme');
  $export['night_full_moon'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'night_time_theme';
  $context->description = '';
  $context->tag = 'theme';
  $context->conditions = array(
    'sun_moon_nighttime' => array(
      'values' => array(
        'sun_moon_nighttime' => '2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sun_moon-moon_shine' => array(
          'module' => 'sun_moon',
          'delta' => 'moon_shine',
          'region' => 'moonspace',
          'weight' => '-10',
        ),
        'sun_moon-moon_report' => array(
          'module' => 'sun_moon',
          'delta' => 'moon_report',
          'region' => 'postscript_first',
          'weight' => '-23',
        ),
        'sun_moon-sun_report' => array(
          'module' => 'sun_moon',
          'delta' => 'sun_report',
          'region' => 'postscript_second',
          'weight' => '-10',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'nighttime',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('theme');
  $export['night_time_theme'] = $context;

  return $export;
}
