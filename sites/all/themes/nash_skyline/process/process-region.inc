<?php

function nash_skyline_alpha_process_region(&$vars) {
  if (in_array($vars['elements']['#region'], array('user_first', 'user_second'))) {
    $theme = alpha_get_theme();
    
    switch ($vars['elements']['#region']) {

      case 'user_first':
        $vars['main_menu'] = $theme->page['main_menu'];
        break;

      case 'user_second':
        $vars['user_menu'] = $theme->page['secondary_menu'];
        break;
      
    }
  }
}
