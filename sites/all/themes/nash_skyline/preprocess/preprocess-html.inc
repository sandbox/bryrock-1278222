<?php

function nash_skyline_alpha_preprocess_html(&$vars) {
	//  See if the daytime theme is in use, then use the daytime logo
  if (array_search('nash-sky-day', $vars['classes_array']) != FALSE) {
  	variable_set('sun_moon_switch_logo','/sites/all/themes/nash_skyline/images/ND_sunny_logo.png');
  } 
  else { // otherwise use the default (night logo)
  	variable_set('sun_moon_switch_logo','/sites/all/themes/nash_skyline/images/ND_moony_logo.png');
 	}
// 	dpm($vars);
}
