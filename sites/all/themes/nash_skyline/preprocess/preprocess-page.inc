<?php

function nash_skyline_alpha_preprocess_page(&$vars) {
  $theme = alpha_get_theme();
  $theme->page = &$vars;
  $vars['logo'] =  variable_get('sun_moon_switch_logo', ($theme->page['logo']));
}
