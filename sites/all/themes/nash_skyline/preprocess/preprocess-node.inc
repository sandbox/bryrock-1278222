<?php

function nash_skyline_alpha_preprocess_node(&$vars) {

  if ($vars['view_mode'] != 'full' && $vars['id'] == 1) {
    $vars['classes_array'][] = 'node-first';
  }

  if ($vars['view_mode'] == 'full' && node_is_page($vars['node'])) {
    $vars['classes_array'][] = 'node-full';
  }
 
}
