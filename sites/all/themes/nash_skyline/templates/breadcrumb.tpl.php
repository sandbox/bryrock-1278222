<?php 
/**
 * @file
 * Welcome visitors when there is no breadcrumb.  This allows us
 * to reserve that layout space when not in use by actual breadcrumb.
 */
?>
<?php if (!empty($breadcrumb)):?>
  <div class="breadcrumb"><?php print implode('  »  ' , $breadcrumb) ?></div>
<?php else: ?>
<div class="breadcrumb ">Welcome to Nashville Drupalers!</div>
<?php endif ; ?>