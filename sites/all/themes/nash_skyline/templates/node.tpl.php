<div class="<?php print $classes; ?>">
  <div class="node-inner clearfix">
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    		<?php // krumo($content) ?>

    <?php print render($content['group_post_lede']); ?>

    <div class="meta">
      <?php if ($display_submitted): ?>
        <?php // print $user_picture; ?>
        <?php print t('Published by '); ?><?php print $name.t(' on ').$date; ?>
        <?php if (!empty($content['field_tags'])): ?>
          <?php print ' in '.render($content['field_tags']);  ?>
        <?php endif; ?>
     <?php endif; ?>
    </div>

    <div class="content clearfix">
    <?php
      hide($content['comments']);
      hide($content['links']);
	  	hide($content['field_tags']);
      print render($content);
      ?>
      
      <?php if (!$display_submitted): ?>
        <div class="meta-after">
        <?php print 'Found in '.render($content['field_tags']);  ?>
        </div>
	  <?php endif; ?>
    </div>

    <?php if ($content['links']): ?>
      <div class="extra-links">
	    <?php if (!$page): ?>
	      <div class="read-more"><?php print l(t('Read more'), 'node/' . $nid, array('attributes' => array('class' => array (t('node-readmore-link'))))); ?></div>
		<?php endif; ?>
        <?php print render($content['links']); ?>
      </div>
	<?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</div> <!-- /node-inner, /node -->