<?php 
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
?>

<?php // global $theme_info; krumo($theme_info); ?>
<?php // krumo($variables); ?>
<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>
  
  <?php if (isset($page['content'])) : ?>
    <?php print render($page['content']); ?>
  <?php endif; ?>  
  
  <?php if (isset($page['footer'])) : ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>
</div>